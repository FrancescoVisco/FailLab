﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlinePlatform : MonoBehaviour
{

    public bool Overheating;
    public bool Cooling;
    public bool Growing;

    public GameObject OverOutline;
    public GameObject CoolOutline;
    public GameObject GrowOutline;

    void Start()
    {
        
    }

    void Update()
    {
        if(Overheating == true)
        {
            OverOutline.SetActive(true);
            CoolOutline.SetActive(false);
            GrowOutline.SetActive(false);
        }
        else if(Cooling == true)
        {
            OverOutline.SetActive(false);
            CoolOutline.SetActive(true);
            GrowOutline.SetActive(false);           
        }
        else if(Growing == true)
        {
            OverOutline.SetActive(false);
            CoolOutline.SetActive(false);
            GrowOutline.SetActive(true); 
        }
        else
        {
            OverOutline.SetActive(false);
            CoolOutline.SetActive(false);
            GrowOutline.SetActive(false);         
        }
    }
}
