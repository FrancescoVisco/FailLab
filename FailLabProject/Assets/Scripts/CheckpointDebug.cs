﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckpointDebug : MonoBehaviour
{
    public bool CoroutineOn;
    public GameObject Model;
    public GameObject Player;
    public GameObject Particle;
    private int ThisScene;

    void Start()
    {
        ThisScene = SceneManager.GetActiveScene().buildIndex;

        if(ThisScene == 2 || ThisScene == 4)
        {
            Model = GameObject.Find("Model");
            Player = GameObject.Find("Robot");
        }
        else
        {
            Model = null;
            Player = null;     
            Particle = null;  
        }
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha0))
        {
            GameObject.Find("LevelLoader").GetComponent<LevelLoader>().SceneToLoad = 0;   
            GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;     
        }

        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            GameObject.Find("LevelLoader").GetComponent<LevelLoader>().SceneToLoad = 2;   
            GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;     
        }
    
        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            GameObject.Find("LevelLoader").GetComponent<LevelLoader>().SceneToLoad = 4;   
            GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;     
        }

        if(Input.GetKeyDown(KeyCode.L))
        {
            GameObject.Find("GameController").GetComponent<GameManager>().Lifes +=1;      
        }

        if(Input.GetKeyDown(KeyCode.C))
        {
            CoroutineOn = true;   
        }

        if(CoroutineOn == true)
        {
            StartCoroutine(DelayDeath());
        }
        else
        {
            StopCoroutine(DelayDeath());
        }
    }

    IEnumerator DelayDeath()
    {
        Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        Model.SetActive(false);
        Particle.transform.position = new Vector3 (Player.transform.position.x,Player.transform.position.y+1,Player.transform.position.z);
        Particle.SetActive(true);
        yield return new WaitForSeconds(0.5f); 
        GameObject.Find("LevelLoader").GetComponent<LevelLoader>().FadeRespawn = true; 
        yield return new WaitForSeconds(2.0f);      
        Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        Player.gameObject.transform.position = GameManager.Instance.lastCheckPoint.position;
        Particle.SetActive(false);
        Model.SetActive(true);
        CoroutineOn = false;  
    }
}
