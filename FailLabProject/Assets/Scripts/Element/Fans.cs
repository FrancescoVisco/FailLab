﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fans : MonoBehaviour
{
    public float FansForce;
    public float DownForce;

    void Start()
    {   

    }

    void FixedUpdate()
    {
        
    }

    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {

            other.GetComponent<Rigidbody>().AddForce(transform.up * FansForce, ForceMode.Acceleration);
         
        }
        else if(other.gameObject.tag == "Box")
        {
            other.GetComponent<Rigidbody>().AddForce(transform.up * FansForce, ForceMode.Acceleration);
        }
        
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.GetComponent<Rigidbody>().velocity = new Vector3(0,DownForce,0);
        }
        else if(other.gameObject.tag == "Box")
        {
            other.GetComponent<Rigidbody>().velocity = new Vector3(0,DownForce,0);
        }
    }
}
