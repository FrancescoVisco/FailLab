﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public GameObject FloatingText;
    public Transform SpawnPoint;
    public Vector3 Position;
    public Quaternion Rotation;

    public AudioSource Key;
    public AudioClip Click;

    void Start()
    {
        Position = SpawnPoint.transform.position;
        Rotation = SpawnPoint.transform.rotation;
    }

    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Key.PlayOneShot(Click, 0.7F);
            Instantiate(FloatingText, Position, Rotation);
            GameManager.Instance.lastCheckPoint = transform;
        }
    }
}
