﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RKey : MonoBehaviour
{
    public GameObject[] ObjectToRotate;
    public bool CubeOn;
    public bool PlayerOn;
    public bool OneObj;
    public bool TwoObj;
    public bool ThreeObj;

    public GameObject FloatingText;
    public Transform SpawnPoint;
    public Vector3 Position;
    public Quaternion Rotation;
    public AudioSource Key;
    public AudioClip Click;
    public AudioSource RotObj;
    public AudioClip RotSound;

    void Start()
    {
        Position = SpawnPoint.transform.position;
        Rotation = SpawnPoint.transform.rotation;
    }

    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {      
        if(other.gameObject.tag == "Player" && CubeOn == false)
        {   
            Instantiate(FloatingText, Position, Rotation); 
            Key.PlayOneShot(Click, 0.7F);
            RotObj.PlayOneShot(RotSound, 0.2F);
            if(OneObj == true)
            {
                StartCoroutine(ObjectToRotate[0].GetComponent<RObject>().RotateAxisY());
                PlayerOn = true;
            }
            else if(TwoObj == true)
            {
                StartCoroutine(ObjectToRotate[0].GetComponent<RObject>().RotateAxisY());
                StartCoroutine(ObjectToRotate[1].GetComponent<RObject>().RotateAxisY());
                PlayerOn = true;               
            }
            else if(ThreeObj == true)
            {
                StartCoroutine(ObjectToRotate[0].GetComponent<RObject>().RotateAxisY());
                StartCoroutine(ObjectToRotate[1].GetComponent<RObject>().RotateAxisY());
                StartCoroutine(ObjectToRotate[2].GetComponent<RObject>().RotateAxisY());
                PlayerOn = true;       
            }
        }

        if(other.gameObject.tag == "Box" && PlayerOn == false)
        
            if(OneObj == true)
            {
                StartCoroutine(ObjectToRotate[0].GetComponent<RObject>().RotateAxisY());
                CubeOn = true;
            }
            else if(TwoObj == true)
            {
                StartCoroutine(ObjectToRotate[0].GetComponent<RObject>().RotateAxisY());
                StartCoroutine(ObjectToRotate[1].GetComponent<RObject>().RotateAxisY());
                CubeOn = true;               
            }
            else if(ThreeObj == true)
            {
                StartCoroutine(ObjectToRotate[0].GetComponent<RObject>().RotateAxisY());
                StartCoroutine(ObjectToRotate[1].GetComponent<RObject>().RotateAxisY());
                StartCoroutine(ObjectToRotate[2].GetComponent<RObject>().RotateAxisY());
                CubeOn = true;       
            }
        }

    void OnTriggerExit(Collider other)
    { 
        if(other.gameObject.tag == "Player")
        {
            PlayerOn = false;
        }

        if(other.gameObject.tag == "Box")
        {
            CubeOn = false;
        }
    }
}
