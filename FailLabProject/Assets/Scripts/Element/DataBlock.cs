using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBlock : MonoBehaviour
{
    [Header("Boolean Block Settings")]
    public bool MinDim;
    public bool BaseDim;
    public bool MaxDim;
    public bool CoroutineOn;
    public bool horizontal;

    [Header("Block Dimension Settings")]
    public float t;
    public float scaleDuration;  
    public Vector3 actualScale;
    public Vector3 targetScale;
    public Vector3 Min;
    public Vector3 Base;
    public Vector3 Max;

    [Header("Block Dimension Settings")]
    public Material DataMat;
    public Color Green;
    public Color Orange;
    public Color Red;
    public Color startColor;
    public Color targetColor;

    [Header("Block Dimension Settings")]
    public AudioSource BlockData;
    public AudioClip Block;

    void Start()
    {
        
    }

    void FixedUpdate()
    {
        actualScale = this.gameObject.transform.localScale;
        DataMat = this.GetComponent<MeshRenderer>().material;
        startColor = DataMat.color;

        //Scale Size and Color Lerp Target
        if(MinDim == true)
        {
            targetScale = Min;
            targetColor = Green;
            StartCoroutine(ScaleObject());
        }
        else if(BaseDim == true)
        {
            targetScale = Base;
            targetColor = Orange;
            StartCoroutine(ScaleObject());
        }
        else if(MaxDim == true)
        {
            targetScale = Max;
            targetColor = Red;
            StartCoroutine(ScaleObject());
        }

        if(CoroutineOn == true)
        {
            for(t = 0; t < 0.15f; t += Time.deltaTime/scaleDuration)
            {   
            this.gameObject.transform.localScale = Vector3.Lerp(actualScale, targetScale ,t);
            DataMat.SetColor("_Color", Color.Lerp(startColor, targetColor, t));
            }
        }      
    }

    IEnumerator ScaleObject()
    {
        CoroutineOn = true;
        yield return new WaitForSeconds(0.2f);
        CoroutineOn = false;   
    }
}
