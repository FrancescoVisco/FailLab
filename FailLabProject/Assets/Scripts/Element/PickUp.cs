﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public float speed;
    public float height;
    public bool Tutorial;
    public GameObject LevelLoader;
    public GameObject CanvasUI;
    public AudioSource LifeSource;
    public AudioClip Life;

    Vector3 pos;
    
    void Start()
    {
        pos = transform.position;  
    }

    void Update()
    {
        //calculate what the new Y position will be
        float newY = Mathf.Sin(Time.time * speed) * height + pos.y;
        //set the object's Y to the new calculated Y
        transform.position = new Vector3(transform.position.x, newY, transform.position.z) ;

        if(GameObject.Find("CanvasPause").GetComponent<PauseMenu>().GameIsPaused == false)
        {
            speed = 0;
        }
        else
        {
            speed = 2;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && Tutorial == false)
        {
            GameObject.Find("GameController").GetComponent<GameManager>().Lifes += 1;
            AudioSource.PlayClipAtPoint(Life, transform.position);
            Destroy(this.gameObject);
        }
        
        if (other.gameObject.tag == "Player" && Tutorial == true)
        {
            GameObject.Find("GameController").GetComponent<GameManager>().Lifes = 3;
            LevelLoader.SetActive(true);
            CanvasUI.SetActive(true);
            AudioSource.PlayClipAtPoint(Life, transform.position);
            Destroy(this.gameObject);
        }
    }
}
