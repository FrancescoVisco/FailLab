﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour
{
    [Header("Basic Platform Settings")]
    public Transform[] target;
    public int current = 0;
    float WPradius = 1;
    public bool PlatformIsActive;
    public GameObject Obj;

    [Header("Speed Settings")]
    public float speed;  
    public float minspeed;
    public float basespeed;
    public float maxspeed;
    public bool accelerate;
    public bool decelerate;
    public bool basic;

    [Header("Size Settings")]
    public bool MinDim;
    public bool BaseDim;
    public bool MaxDim;
    public bool CoroutineOn;
    public float t;
    public float scaleDuration;  
    public Vector3 actualScale;
    public Vector3 targetScale;
    public Vector3 Min;
    public Vector3 Base;
    public Vector3 Max;

    [Header("Particle Settings")]
    public GameObject Particle0;
    public GameObject Particle1;
    public GameObject Particle2;

    void Start()
    {
        if(accelerate == true)
        {
            speed = maxspeed;
        }
        else if(decelerate == true)
        {
            speed = minspeed;
        }
        else if(basic == true)
        {
            speed = basespeed;
        }
    }

    void FixedUpdate()
    {
        //Change Speed
        if (PlatformIsActive == true)
        {
            if (Vector3.Distance(target[current].transform.position, transform.position) < WPradius)
            {
                current = Random.Range(0, target.Length);

                if (current >= target.Length)
                {
                    current = 0;
                }
            }
            transform.position = Vector3.MoveTowards(transform.position, target[current].transform.position, Time.deltaTime * speed);
        }

        //Change Size
        actualScale = this.gameObject.transform.localScale;

        if(MinDim == true)
        {
            targetScale = Min;
            StartCoroutine(ScaleObject());
        }
        else if(BaseDim == true)
        {
            targetScale = Base;
            StartCoroutine(ScaleObject());
        }
        else if(MaxDim == true)
        {
            targetScale = Max;
            StartCoroutine(ScaleObject());
        }
        if(CoroutineOn == true)
        {
            for(t = 0; t < 0.15f; t += Time.deltaTime/scaleDuration)
            {
            this.gameObject.transform.localScale = Vector3.Lerp(actualScale, targetScale ,t);
            }
        }

        //Particle Changer
        if(speed == minspeed)
        {
            Particle0.SetActive(true);
            Particle1.SetActive(false);
            Particle2.SetActive(false);           
        }
        else if(speed == basespeed)
        {
            Particle0.SetActive(false);
            Particle1.SetActive(true);
            Particle2.SetActive(false);            
        }
        else if(speed == maxspeed)
        {
            Particle0.SetActive(false);
            Particle1.SetActive(false);
            Particle2.SetActive(true);              
        }
    }

    IEnumerator ScaleObject()
    {
        CoroutineOn = true;
        yield return new WaitForSeconds(0.2f);
        CoroutineOn = false;   
    }

    //Parent Settings
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Player")
        {
            Obj = other.gameObject;
            PlatformIsActive = true;
            Obj.transform.SetParent(this.gameObject.transform, true);
            GameObject.Find("Robot").GetComponent<FixScale>().parent = this.gameObject;
        }
        else if(other.gameObject.tag == "Box")
        {
            if(current != 0)
            {
                current = current-1;
            }
            else
            {
                current = current+1;
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            //other.transform.SetParent(this.gameObject.transform, true);
            //GameObject.Find("Robot").GetComponent<FixScale>().parent = this.gameObject;
            //player.transform.parent = transform;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Obj = other.gameObject;
            Obj.transform.SetParent(this.gameObject.transform, true);
        }     
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Obj = other.gameObject;
            Obj.transform.SetParent(null);
            GameObject.Find("Robot").GetComponent<FixScale>().parent = GameObject.Find("Robot");
            //player.transform.localScale = actualScale;
        }
    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            Obj = other.gameObject;
            Obj.transform.SetParent(null);
            GameObject.Find("Robot").GetComponent<FixScale>().parent = GameObject.Find("Robot");
            //player.transform.localScale = actualScale;
        }
    }
}


