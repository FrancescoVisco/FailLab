﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizeBlock : MonoBehaviour
{
    public GameObject Block;
    public float scaleDuration;  
    public float t;  
    public Vector3 basicScale;                 
    public Vector3 actualScale;            
    public Vector3 targetScale;
    public Vector3 LevelTarget;
    public bool LockOn;
    public bool CoroutineOn;

    void Start()
    {
        
    }

    void Update()
    {      
        actualScale = this.gameObject.transform.localScale;

        if(LockOn == true)
        {
            targetScale = LevelTarget;
        }
        else
        {
            targetScale = new Vector3 (1.0f,1.0f,1.0f); 
        }

        if(CoroutineOn == false)
        {
            t = 0;
        }
    }

    public IEnumerator ScaleObject()
    {
        CoroutineOn = true;

        for(t = 0; t < 1; t += Time.deltaTime)
        {
            this.gameObject.transform.localScale = Vector3.Lerp(actualScale ,targetScale ,t);
            yield return null;
        }    

        if(t > 0)
        {
            CoroutineOn = false;
        }
    }
}
