﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsLock : MonoBehaviour
{
    public GameObject[] Block;
    public bool CubeOn;
    public bool PlayerOn;
    public bool OneObj;
    public bool TwoObj;
    public bool ThreeObj;

    public GameObject FloatingText;
    public Transform SpawnPoint;
    public Vector3 Position;
    public Quaternion Rotation;

    public AudioSource Key;
    public AudioClip Click;
    public AudioSource Size;
    public AudioClip BlockSound;


    void Start()
    {
        Position = SpawnPoint.transform.position;
        Rotation = SpawnPoint.transform.rotation;
    }

    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" && CubeOn == false)
        {          
            Instantiate(FloatingText, Position, Rotation);
            Key.PlayOneShot(Click, 0.7F); 
            Size.PlayOneShot(BlockSound, 0.5F);   
            if(OneObj == true)
            {
                StartCoroutine(Block[0].GetComponent<SizeBlock>().ScaleObject());
                Block[0].GetComponent<SizeBlock>().LockOn = !Block[0].GetComponent<SizeBlock>().LockOn;
                PlayerOn = true;
            }
            else if(TwoObj == true)
            {
                StartCoroutine(Block[0].GetComponent<SizeBlock>().ScaleObject());
                StartCoroutine(Block[1].GetComponent<SizeBlock>().ScaleObject());
                Block[0].GetComponent<SizeBlock>().LockOn = !Block[0].GetComponent<SizeBlock>().LockOn;
                Block[1].GetComponent<SizeBlock>().LockOn = !Block[0].GetComponent<SizeBlock>().LockOn;
                PlayerOn = true;  
            }
            else if(ThreeObj == true)
            {
                StartCoroutine(Block[0].GetComponent<SizeBlock>().ScaleObject());
                StartCoroutine(Block[1].GetComponent<SizeBlock>().ScaleObject());
                StartCoroutine(Block[2].GetComponent<SizeBlock>().ScaleObject());
                Block[0].GetComponent<SizeBlock>().LockOn = !Block[0].GetComponent<SizeBlock>().LockOn;
                Block[1].GetComponent<SizeBlock>().LockOn = !Block[1].GetComponent<SizeBlock>().LockOn;
                Block[2].GetComponent<SizeBlock>().LockOn = !Block[2].GetComponent<SizeBlock>().LockOn;
                PlayerOn = true;
            }
        }

        if(other.gameObject.tag == "Box" && PlayerOn == false)
        { 
            if(OneObj == true)
            {
                StartCoroutine(Block[0].GetComponent<SizeBlock>().ScaleObject());
                Block[0].GetComponent<SizeBlock>().LockOn = !Block[0].GetComponent<SizeBlock>().LockOn;
                CubeOn = true;
            }
            else if(TwoObj == true)
            {
                StartCoroutine(Block[0].GetComponent<SizeBlock>().ScaleObject());
                StartCoroutine(Block[1].GetComponent<SizeBlock>().ScaleObject());
                Block[0].GetComponent<SizeBlock>().LockOn = !Block[0].GetComponent<SizeBlock>().LockOn;
                Block[1].GetComponent<SizeBlock>().LockOn = !Block[0].GetComponent<SizeBlock>().LockOn;
                CubeOn = true;     
            }
            else if(ThreeObj == true)
            {
                StartCoroutine(Block[0].GetComponent<SizeBlock>().ScaleObject());
                StartCoroutine(Block[1].GetComponent<SizeBlock>().ScaleObject());
                StartCoroutine(Block[2].GetComponent<SizeBlock>().ScaleObject());
                Block[0].GetComponent<SizeBlock>().LockOn = !Block[0].GetComponent<SizeBlock>().LockOn;
                Block[1].GetComponent<SizeBlock>().LockOn = !Block[1].GetComponent<SizeBlock>().LockOn;
                Block[2].GetComponent<SizeBlock>().LockOn = !Block[2].GetComponent<SizeBlock>().LockOn;
                CubeOn = true;  
            }  
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {           
            PlayerOn = false;
        }

        if(other.gameObject.tag == "Box")
        { 
            CubeOn = false;   
        }
    }
}