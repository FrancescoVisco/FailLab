﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FansController : MonoBehaviour
{
    public GameObject Trigger0;
    public GameObject Trigger1;
    public GameObject Trigger2;
    public bool accelerate;
    public bool decelerate;
    public bool basic;

    void Start()
    {
        if(accelerate == true)
        {
            Trigger0.SetActive(false);
            Trigger1.SetActive(false);
            Trigger2.SetActive(true);
        }
        else if(decelerate == true)
        {
            Trigger0.SetActive(true);
            Trigger1.SetActive(false);
            Trigger2.SetActive(false);            
        }
        else if(basic == true)
        {
            Trigger0.SetActive(false);
            Trigger1.SetActive(true);
            Trigger2.SetActive(false);  
        }    
    }

    void Update()
    {

    }
}
