﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RObject : MonoBehaviour
{
    public GameObject Key;
    public bool CoroutineOn;

    void Start()
    {
        
    }

    void Update()
    {

    }

    public IEnumerator RotateAxisY ()
    {
            CoroutineOn = true; 
            var currentRotation = this.gameObject.transform.localRotation;
            var newRotation = currentRotation * Quaternion.AngleAxis (90, Vector3.up);
        
            for (float t = 0;t<0.5f;t += Time.deltaTime)
            {
               Quaternion rotation = Quaternion.Slerp (currentRotation, newRotation, t * 2);
               this.gameObject.transform.localRotation = rotation;
               yield return null;
            }
            this.gameObject.transform.localRotation = newRotation;
            yield return new WaitForSeconds(0.3f);
            CoroutineOn = false;
    }        
}

