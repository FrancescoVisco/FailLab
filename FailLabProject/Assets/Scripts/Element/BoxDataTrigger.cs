﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxDataTrigger : MonoBehaviour
{

    public GameObject Door;
    public GameObject Door1;
    public AudioSource DoorSource;
    public AudioClip Open;
    public AudioClip Close;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Box")
        { 
            DoorSource.PlayOneShot(Open, 0.3f);
            Door.SetActive(false);
        }

        if(other.gameObject.tag == "Player")
        { 
            DoorSource.PlayOneShot(Open, 0.3f);

            if(Door.activeSelf == true)
            {
                Door.SetActive(false);
            }
            else
            {
                Door1.SetActive(false);
            }        
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Box")
        { 
            DoorSource.PlayOneShot(Close, 0.5f);
            Door.SetActive(true);
        }
    }

}
