﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterKey : MonoBehaviour
{
    [Header("Dvd Player Settings")]
    public float speed;
    public GameObject DvdPlayer;
    int i = 0;
    public Transform[] target;
    public GameObject key;
    public Vector3 actualScale;
    public Vector3 targetScale;
    public bool Opened;

    public GameObject FloatingText;
    public Transform SpawnPoint;
    public Vector3 Position;
    public Quaternion Rotation;

    public AudioSource Key;
    public AudioClip Click;
    public AudioSource DvdSource;
    public AudioClip DvdSound;

    public float t;

    void Start()
    {
        Position = SpawnPoint.transform.position;
        Rotation = SpawnPoint.transform.rotation;      
    }

    void Update()
    {
        DvdPlayer.transform.position = Vector3.MoveTowards(DvdPlayer.transform.position, target[i].transform.position, Time.deltaTime * speed);

        if(Opened == true)
        {
            StartCoroutine(Open());
            i = 1;
        }
        else
        {
            StopAllCoroutines();
            i = 0;        
        }
    }
    
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" || other.gameObject.tag == "Box")
        {
            Instantiate(FloatingText, Position, Rotation);
            Key.PlayOneShot(Click, 0.7F);
            DvdSource.PlayOneShot(DvdSound, 0.7F);
            
            if(Opened == false)
            {
                Opened = true;

                if(key != null)
                {
                   key.GetComponent<EnterKey>().Opened = true;  
                }               
            }     
        }
    }

    IEnumerator Open()
    {
        yield return new WaitForSeconds(t + 10.0f);
        Opened = false;
    }
}
