﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CorruptedTerrain : MonoBehaviour
{
    [Header("Ability Controller Settings")]
    public int Life;
    public GameObject Player;
    public GameObject TriggerAbility;
    public bool Overheat;
    public bool Cool;
    public bool Grow;

    [Header("Death and Particle Settings")]
    public GameObject Model;
    public GameObject Particle;
    public bool CoroutineOn;

    void Update()
    {
        Life = GameObject.Find("GameController").GetComponent<GameManager>().Lifes;

        if(CoroutineOn == true)
        {
            StartCoroutine(DelayDeath());
        }
        else
        {
            StopCoroutine(DelayDeath());
            //GameObject.Find("LevelLoader").GetComponent<LevelLoader>().FadeRespawn = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if((Overheat == true && TriggerAbility.GetComponent<Ability>().Overheating == false) || (Cool == true && TriggerAbility.GetComponent<Ability>().Cooling == false) || (Grow == true && TriggerAbility.GetComponent<Ability>().Growing == false))
            {
                if(Life > 0)
                {
                    GameObject.Find("GameController").GetComponent<GameManager>().Lifes -= 1;
                    CoroutineOn = true;    

                    if(Overheat == true)
                    {
                        GameObject.Find("TriggerAbility").GetComponent<Ability>().Overheating = true;
                        GameObject.Find("TriggerAbility").GetComponent<Ability>().Cooling = false;
                        GameObject.Find("TriggerAbility").GetComponent<Ability>().Growing = false;
                    }
                    else if(Cool == true)
                    {
                        GameObject.Find("TriggerAbility").GetComponent<Ability>().Overheating = false;
                        GameObject.Find("TriggerAbility").GetComponent<Ability>().Cooling = true;
                        GameObject.Find("TriggerAbility").GetComponent<Ability>().Growing = false;
                    }
                    else if(Grow == true)
                    {
                        GameObject.Find("TriggerAbility").GetComponent<Ability>().Overheating = false;
                        GameObject.Find("TriggerAbility").GetComponent<Ability>().Cooling = false;
                        GameObject.Find("TriggerAbility").GetComponent<Ability>().Growing = true;
                    }
                }
                else
                {
                    StartCoroutine(Death());
                }   
            }
        }
    }

    IEnumerator DelayDeath()
    {   
        Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        Model.SetActive(false);
        Particle.transform.position = new Vector3 (Player.transform.position.x,Player.transform.position.y+1,Player.transform.position.z);
        Particle.SetActive(true);
        yield return new WaitForSeconds(0.5f); 
        GameObject.Find("LevelLoader").GetComponent<LevelLoader>().FadeRespawn = true; 
        yield return new WaitForSeconds(2.0f);      
        Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        Player.gameObject.transform.position = GameManager.Instance.lastCheckPoint.position;
        Particle.SetActive(false);
        Model.SetActive(true);
        CoroutineOn = false;  
    }

    IEnumerator Death()
    {   
        Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        Model.SetActive(false);
        Particle.transform.position = new Vector3 (Player.transform.position.x,Player.transform.position.y+1,Player.transform.position.z);
        Particle.SetActive(true);
        yield return new WaitForSeconds(0.4f);  
        Particle.SetActive(false);    
        yield return new WaitForSeconds(0.4f);  
        GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true; 
    }
}
