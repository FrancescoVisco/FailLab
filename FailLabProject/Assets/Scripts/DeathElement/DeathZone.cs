﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathZone : MonoBehaviour
{

    public int Life;
    public GameObject Player;
    public GameObject Model;
    public GameObject Particle;
    public bool CoroutineOn;

    void Update()
    {
        Life = GameObject.Find("GameController").GetComponent<GameManager>().Lifes;
        Player = GameObject.FindGameObjectWithTag("Player");

        if(CoroutineOn == true)
        {
            StartCoroutine(DelayDeath());
        }
        else
        {
            StopCoroutine(DelayDeath());
            //GameObject.Find("LevelLoader").GetComponent<LevelLoader>().FadeRespawn = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if(Life > 0)
            {
                GameObject.Find("GameController").GetComponent<GameManager>().Lifes -= 1;
                CoroutineOn = true;                 
            }
            else
            {
                StartCoroutine(Death());
            }   
        }
    }

    IEnumerator DelayDeath()
    {   
        Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        Model.SetActive(false);
        Particle.transform.position = Player.transform.position;
        Particle.SetActive(true);
        yield return new WaitForSeconds(0.5f); 
        GameObject.Find("LevelLoader").GetComponent<LevelLoader>().FadeRespawn = true; 
        yield return new WaitForSeconds(2.0f);      
        Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        Player.gameObject.transform.position = GameManager.Instance.lastCheckPoint.position;
        Particle.SetActive(false);
        Model.SetActive(true);
        CoroutineOn = false;  
    }

    IEnumerator Death()
    {   
        Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        Model.SetActive(false);
        Particle.transform.position = Player.transform.position;
        Particle.SetActive(true);
        yield return new WaitForSeconds(0.4f);  
        Particle.SetActive(false);    
        yield return new WaitForSeconds(0.4f);  
        GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true; 
    }
}