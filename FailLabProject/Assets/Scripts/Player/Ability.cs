﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : MonoBehaviour
{
    [Header("Ability Controller Settings")]
    public bool Overheating;
    public bool Cooling;
    public bool Growing;
    public bool Standard;
    public bool SoundCheck;
    public GameObject Object;

    private AudioSource AudioSource;
    private AudioClip AudioClip;
    public AudioSource AbilitySource;
    public AudioClip AbilityClip;

    void Start()
    {
        
    }

    void Update()
    {
        //ActivationAbility
        if((Input.GetButtonDown("Fire1")) && Overheating == true)
        {
            Overheat();
        }

        if(Input.GetButtonDown("Fire1") && Cooling == true)
        {
            Cool();
        }

        if(Input.GetButtonDown("Fire1") && Growing == true)
        {
            Grow();
        }

        if(Cooling == true && Overheating == true && Growing == true)
        {
            Standard = false;
        }
        else
        {
            Standard = true;
        }

        //Materials Controller
        if(Overheating == true)
        {
            GameObject.Find("Robot").GetComponent<MaterialController>().Red = true;
            GameObject.Find("Robot").GetComponent<MaterialController>().Green = false;
            GameObject.Find("Robot").GetComponent<MaterialController>().Blue = false;
        }else if(Cooling == true)
        {
            GameObject.Find("Robot").GetComponent<MaterialController>().Red = false;
            GameObject.Find("Robot").GetComponent<MaterialController>().Green = false;
            GameObject.Find("Robot").GetComponent<MaterialController>().Blue = true;
        }else if(Growing == true)
        {
            GameObject.Find("Robot").GetComponent<MaterialController>().Red = false;
            GameObject.Find("Robot").GetComponent<MaterialController>().Green = true;
            GameObject.Find("Robot").GetComponent<MaterialController>().Blue = false;
        }
        else
        {
            GameObject.Find("Robot").GetComponent<MaterialController>().Red = false;
            GameObject.Find("Robot").GetComponent<MaterialController>().Green = false;
            GameObject.Find("Robot").GetComponent<MaterialController>().Blue = false; 
        }
    }

    public void Overheat()
    {
        if(Object.gameObject.tag == "Platform")
        {
            AbilitySource.PlayOneShot(AbilityClip, 1.0f);
            if(Object.GetComponent<MovePlatform>().speed == Object.GetComponent<MovePlatform>().minspeed)
            {
                Object.GetComponent<MovePlatform>().speed = Object.GetComponent<MovePlatform>().basespeed;
            }
            else if(Object.GetComponent<MovePlatform>().speed == Object.GetComponent<MovePlatform>().basespeed)
            {
                Object.GetComponent<MovePlatform>().speed = Object.GetComponent<MovePlatform>().maxspeed;
            }
            else if(Object.GetComponent<MovePlatform>().speed == Object.GetComponent<MovePlatform>().maxspeed)
            {
                Object.GetComponent<MovePlatform>().speed = Object.GetComponent<MovePlatform>().basespeed;
            }
        }
        else if(Object.gameObject.tag == "Fan")
        {
            AbilitySource.PlayOneShot(AbilityClip, 1.0f);
            if(Object.GetComponent<FansController>().Trigger0.activeSelf)
            {
               Object.GetComponent<FansController>().Trigger0.SetActive(false);
               Object.GetComponent<FansController>().Trigger1.SetActive(true);
               Object.GetComponent<FansController>().Trigger2.SetActive(false);
            
            }else if(Object.GetComponent<FansController>().Trigger1.activeSelf)
            {
               Object.GetComponent<FansController>().Trigger0.SetActive(false);
               Object.GetComponent<FansController>().Trigger1.SetActive(false);
               Object.GetComponent<FansController>().Trigger2.SetActive(true);
            }
            else if(Object.GetComponent<FansController>().Trigger2.activeSelf)
            {
               Object.GetComponent<FansController>().Trigger0.SetActive(false);
               Object.GetComponent<FansController>().Trigger1.SetActive(true);
               Object.GetComponent<FansController>().Trigger2.SetActive(false);
            }
        }
        else if(Object.gameObject.tag == "CPU")
        { 
           Object.GetComponent<EndCpu>().i += 1;
           AbilitySource.PlayOneShot(AbilityClip, 1.0f);
        } 
    }

    public void Cool()
    {
        if(Object.gameObject.tag == "Platform")
        {
            AbilitySource.PlayOneShot(AbilityClip, 1.0f);
            if(Object.GetComponent<MovePlatform>().speed == Object.GetComponent<MovePlatform>().maxspeed)
            {
                Object.GetComponent<MovePlatform>().speed = Object.GetComponent<MovePlatform>().basespeed;
            }
            else if(Object.GetComponent<MovePlatform>().speed == Object.GetComponent<MovePlatform>().basespeed)
            {
                Object.GetComponent<MovePlatform>().speed = Object.GetComponent<MovePlatform>().minspeed;
            }
            else if(Object.GetComponent<MovePlatform>().speed == Object.GetComponent<MovePlatform>().minspeed)
            {
                Object.GetComponent<MovePlatform>().speed = Object.GetComponent<MovePlatform>().basespeed;
            }
        }      
        else if(Object.gameObject.tag == "Fan")
        {
            AbilitySource.PlayOneShot(AbilityClip, 1.0f);
            if(Object.GetComponent<FansController>().Trigger2.activeSelf)
            {
               Object.GetComponent<FansController>().Trigger0.SetActive(false);
               Object.GetComponent<FansController>().Trigger1.SetActive(true);
               Object.GetComponent<FansController>().Trigger2.SetActive(false);
            }else if(Object.GetComponent<FansController>().Trigger1.activeSelf)
            {
               Object.GetComponent<FansController>().Trigger0.SetActive(true);
               Object.GetComponent<FansController>().Trigger1.SetActive(false);
               Object.GetComponent<FansController>().Trigger2.SetActive(false);
            }else if(Object.GetComponent<FansController>().Trigger0.activeSelf)
            {
               Object.GetComponent<FansController>().Trigger0.SetActive(false);
               Object.GetComponent<FansController>().Trigger1.SetActive(true);
               Object.GetComponent<FansController>().Trigger2.SetActive(false);
            }
        }
        else if(Object.gameObject.tag == "CPU")
        { 
           Object.GetComponent<EndCpu>().i += 1;
           AbilitySource.PlayOneShot(AbilityClip, 1.0f);
        } 
    }

    public void Grow()
    {
       if(Object.gameObject.tag == "DataBlock") 
       {
           if(Object.GetComponent<DataBlock>().MinDim == true)
           {
              Object.GetComponent<DataBlock>().MinDim = false;
              Object.GetComponent<DataBlock>().BaseDim = true;
              Object.GetComponent<DataBlock>().MaxDim = false;
           }
           else if(Object.GetComponent<DataBlock>().BaseDim == true)
           {
              Object.GetComponent<DataBlock>().MinDim = false;
              Object.GetComponent<DataBlock>().BaseDim = false;
              Object.GetComponent<DataBlock>().MaxDim = true;
           }
           else if(Object.GetComponent<DataBlock>().MaxDim == true)
           {
              Object.GetComponent<DataBlock>().MinDim = true;
              Object.GetComponent<DataBlock>().BaseDim = false;
              Object.GetComponent<DataBlock>().MaxDim = false;               
           }
       }

       if(Object.gameObject.tag == "Platform") 
       {
           AbilitySource.PlayOneShot(AbilityClip, 1.0f);
           if(Object.GetComponent<MovePlatform>().MinDim == true)
           {
              Object.GetComponent<MovePlatform>().MinDim = false;
              Object.GetComponent<MovePlatform>().BaseDim = true;
              Object.GetComponent<MovePlatform>().MaxDim = false;
           }
           else if(Object.GetComponent<MovePlatform>().BaseDim == true)
           {
              Object.GetComponent<MovePlatform>().MinDim = false;
              Object.GetComponent<MovePlatform>().BaseDim = false;
              Object.GetComponent<MovePlatform>().MaxDim = true;
           }
           else if(Object.GetComponent<MovePlatform>().MaxDim == true)
           {
              Object.GetComponent<MovePlatform>().MinDim = true;
              Object.GetComponent<MovePlatform>().BaseDim = false;
              Object.GetComponent<MovePlatform>().MaxDim = false;               
           }
       }

        else if(Object.gameObject.tag == "CPU")
        { 
           Object.GetComponent<EndCpu>().i += 1;
           AbilitySource.PlayOneShot(AbilityClip, 1.0f);
        } 
    }

    void OnTriggerStay(Collider other)
    {
        
        if (other.gameObject.tag == "Fan" || other.gameObject.tag == "DataBlock")
        {
            Object = other.gameObject;
            if(Overheating == true)
            {
               other.gameObject.GetComponent<Outline1>().enabled = true;
            }
            else if(Cooling == true)
            {
              other.gameObject.GetComponent<Outline>().enabled = true;
            }
            else if(Growing == true)
            {
              other.gameObject.GetComponent<Outline2>().enabled = true;
            }
        }
        else if(other.gameObject.tag == "Platform")
        {
            Object = other.gameObject;
            if(Overheating == true)
            {
                other.gameObject.GetComponent<OutlinePlatform>().Overheating = true;
                other.gameObject.GetComponent<OutlinePlatform>().Cooling = false;
                other.gameObject.GetComponent<OutlinePlatform>().Growing = false;
            }
            else if(Cooling == true)
            {
                other.gameObject.GetComponent<OutlinePlatform>().Overheating = false;
                other.gameObject.GetComponent<OutlinePlatform>().Cooling = true;
                other.gameObject.GetComponent<OutlinePlatform>().Growing = false;
            }
            else if(Growing == true)
            {
                other.gameObject.GetComponent<OutlinePlatform>().Overheating = false;
                other.gameObject.GetComponent<OutlinePlatform>().Cooling = false;
                other.gameObject.GetComponent<OutlinePlatform>().Growing = true;
            }        
        }

        if (other.gameObject.tag == "DataBlock")
        {
            SoundCheck = true;
            if(Input.GetButtonDown("Fire1"))
            {
               AudioSource = other.GetComponent<AudioSource>();
               AudioClip = other.GetComponent<DataBlock>().Block;
               AudioSource.PlayOneShot(AudioClip, 1.0f);
            }
        }

        if (other.gameObject.tag == "CPU")
        {
            Object = other.gameObject;
        }
    }

    void OnTriggerExit(Collider other)
    {
        Object = null;
        if (other.gameObject.tag == "Fan")
        {     
            SoundCheck = false;      
            GameObject.Find("Robot").GetComponent<FixScale>().parent = GameObject.Find("Robot");
            Object = GameObject.Find("Robot");
            other.gameObject.GetComponent<Outline>().enabled = false;
            other.gameObject.GetComponent<Outline1>().enabled = false;
            other.gameObject.GetComponent<Outline2>().enabled = false;
        }

        if (other.gameObject.tag == "Platform")
        {
            Object = GameObject.Find("Robot");
            GameObject.Find("Robot").GetComponent<FixScale>().parent = GameObject.Find("Robot");
            other.gameObject.GetComponent<OutlinePlatform>().Overheating = false;
            other.gameObject.GetComponent<OutlinePlatform>().Cooling = false;
            other.gameObject.GetComponent<OutlinePlatform>().Growing = false;
        }

        if (other.gameObject.tag == "DataBlock")
        {
            Object = GameObject.Find("Robot");
            GameObject.Find("Robot").GetComponent<FixScale>().parent = GameObject.Find("Robot");
            other.gameObject.GetComponent<Outline2>().enabled = false;
        }
    }    
}
