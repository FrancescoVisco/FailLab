﻿ using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;
 
 [ExecuteInEditMode]
 public class FixScale : MonoBehaviour 
 {
    public float FixeScale = 1;
    #pragma warning disable 219
    public GameObject parent;
    #pragma warning restore 219
     
    void Start()
    {

    }

    void Update () 
    {
        if(parent.GetComponent<MovePlatform>() != null)
        {
            transform.localScale = new Vector3 (FixeScale/parent.transform.localScale.x, FixeScale/parent.transform.localScale.y, FixeScale/parent.transform.localScale.z);
        }  
        else if(parent.GetComponent<DataBlock>() != null)
        {
            transform.localScale = new Vector3 (FixeScale/parent.transform.localScale.x, FixeScale/parent.transform.localScale.y, FixeScale/parent.transform.localScale.z);
        }   
    }
}