﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialController : MonoBehaviour
{
    [Header("Robot Material Settings")]
    public bool Green;
    public bool Red;
    public bool Blue;
    public Material MaterialGreen;
    public Material MaterialRed;
    public Material MaterialBlue;
    public Material MaterialWhite;

    public GameObject[] RobotPieces;

    void Start()
    {
        
    }

    void Update()
    {
        if(Green == true)
        {
            for(int i=0; i<RobotPieces.Length;i++)
            {
                RobotPieces[i].GetComponent<SkinnedMeshRenderer>().material = MaterialGreen;
            }
        }
        else if(Blue == true)
        {
            for(int i=0; i<RobotPieces.Length;i++)
            {
                RobotPieces[i].GetComponent<SkinnedMeshRenderer>().material = MaterialBlue;
            }
        }
        else if(Red == true)
        {
            for(int i=0; i<RobotPieces.Length;i++)
            {
                RobotPieces[i].GetComponent<SkinnedMeshRenderer>().material = MaterialRed;
            }
        }
        else
        {
            for(int i=0; i<RobotPieces.Length;i++)
            {
                RobotPieces[i].GetComponent<SkinnedMeshRenderer>().material = MaterialWhite;
            }
        }
    }
}
