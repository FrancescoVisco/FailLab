﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LifeUI: MonoBehaviour
{

    public int Life ;
    public TextMeshProUGUI LifeNumber;

    void Start()
    {
        
    }

    void Update()
    {
        Life = GameObject.Find("GameController").GetComponent<GameManager>().Lifes;
        LifeNumber.text = Life.ToString();
    }
}
