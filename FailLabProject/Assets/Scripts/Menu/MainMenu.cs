﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public GameObject MainPanel;
    public GameObject ControlsPanel;
    public GameObject ControllerPanel;
    public GameObject PCPanel;
    
    void Start () 
    {
        MainPanel.SetActive(true);
        ControlsPanel.SetActive(false);
    }

    void Update()
    {
        
    }

    public void Options()
    {
        MainPanel.SetActive(false);
        ControlsPanel.SetActive(true);
    }

    public void Switch()
    {
        if(ControllerPanel.activeSelf == false)
        {
            PCPanel.SetActive(false);
            ControllerPanel.SetActive(true);
        }
        else
        {
            PCPanel.SetActive(true);
            ControllerPanel.SetActive(false);
        }
    }

    public void Back()
    {
        MainPanel.SetActive(true);
        ControlsPanel.SetActive(false);
        PCPanel.SetActive(false);
        ControllerPanel.SetActive(false);
    }

    public void Play()
    {
        GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
