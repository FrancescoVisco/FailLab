﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelTrigger : MonoBehaviour
{
    private int ThisScene;

    public Transform TargetEnd;
    public float TargetSpeed;
    public GameObject Model;
    
    void Start()
    {
        ThisScene = SceneManager.GetActiveScene().buildIndex;
    }

    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if(ThisScene == 2)
            {
                GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;
                GameObject.Find("LevelLoader").GetComponent<LevelLoader>().transitionTime = 0f;
                GameObject.Find("LevelLoader").GetComponent<LevelLoader>().SceneToLoad = ThisScene+1;
                Model.SetActive(false);
                //GameObject.Find("Robot").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                GameObject.Find("Robot").transform.position = Vector3.MoveTowards(transform.position, TargetEnd.transform.position, TargetSpeed * Time.deltaTime);
            }   
        }
    }
}
