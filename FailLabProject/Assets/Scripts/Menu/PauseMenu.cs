﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{

    public bool GameIsPaused = false;
    public GameObject PauseMenuUI;

    private void Start()
    {

    }

    void Update()
    {
        if (Input.GetButtonDown("pauseButton"))
        {
            if (GameIsPaused == true)
            {
                Resume();
            }
            else
            {            
                Pause();
            }
        }
    }



    public void Resume()
    {
        StartCoroutine(Delay());
        Time.timeScale = 1f;
    }

    public void Pause()
    {
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void ExitToMenu()
    {
        GameObject.Find("LevelLoader").GetComponent<LevelLoader>().SceneToLoad = 0;
        GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.01f);  
        PauseMenuUI.SetActive(false);
        GameIsPaused = false;
    }
}
