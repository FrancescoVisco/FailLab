﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class TypeWriterEffect : MonoBehaviour {

	public float delay = 0.1f;
	public float delaytime;
	public string[] fullText;
	private string currentText0 = "";
	private string currentText1 = "";
	private string currentText2 = "";
	private string currentText3 = "";
	private string currentText4 = "";
	private string currentText5 = "";
	private string currentText6 = "";
	private string currentText7 = "";
	public bool Audio;
	public GameObject Typing;
	public GameObject Image;


	void Start () 
	{
		StartCoroutine(ShowText0());
	}

	void Update()
	{
		if(Audio == true)
		{
			Typing.SetActive(true);
		}
		else
		{
			Typing.SetActive(false);
		}
	}
	
	IEnumerator ShowText0(){

		yield return new WaitForSeconds(delaytime);
		Audio = true;
		Image.SetActive(true);

		for(int i = 0; i <= fullText[0].Length; i++)
		{
			currentText0 = fullText[0].Substring(0,i);
			this.GetComponent<TextMeshProUGUI>().text = currentText0;
			yield return new WaitForSeconds(delay);

			if(i == fullText[7].Length)
			{
				Audio = false;
			}
		}

		yield return new WaitForSeconds(delaytime);
		Audio = true;
		for(int i = 0; i <= fullText[1].Length; i++)
		{
			currentText1 = fullText[1].Substring(0,i);
			GameObject.Find("Text1").GetComponent<TextMeshProUGUI>().text = currentText1;
			yield return new WaitForSeconds(delay);

			if(i == fullText[7].Length)
			{
				Audio = false;
			}
		}

		yield return new WaitForSeconds(delaytime);
		Audio = true;
		for(int i = 0; i <= fullText[2].Length; i++)
		{
			currentText2 = fullText[2].Substring(0,i);
			GameObject.Find("Text2").GetComponent<TextMeshProUGUI>().text = currentText2;
			yield return new WaitForSeconds(delay);

			if(i == fullText[7].Length)
			{
				Audio = false;
			}
		}

		yield return new WaitForSeconds(delaytime);
		Audio = true;
		for(int i = 0; i <= fullText[3].Length; i++)
		{
			currentText3 = fullText[3].Substring(0,i);
			GameObject.Find("Text3").GetComponent<TextMeshProUGUI>().text = currentText3;
			yield return new WaitForSeconds(delay);

			if(i == fullText[7].Length)
			{
				Audio = false;
			}
		}


		yield return new WaitForSeconds(delaytime);
		Audio = true;
		for(int i = 0; i <= fullText[4].Length; i++)
		{
			currentText4 = fullText[4].Substring(0,i);
			GameObject.Find("Text4").GetComponent<TextMeshProUGUI>().text = currentText4;
			yield return new WaitForSeconds(delay);

			if(i == fullText[7].Length)
			{
				Audio = false;
			}
		}

		yield return new WaitForSeconds(delaytime);
		Audio = true;
		for(int i = 0; i <= fullText[5].Length; i++)
		{
			currentText5 = fullText[5].Substring(0,i);
			GameObject.Find("Text5").GetComponent<TextMeshProUGUI>().text = currentText5;
			yield return new WaitForSeconds(delay);

			if(i == fullText[7].Length)
			{
				Audio = false;
			}
		}

		yield return new WaitForSeconds(delaytime);
		Audio = true;
		for(int i = 0; i <= fullText[6].Length; i++)
		{
			currentText6 = fullText[6].Substring(0,i);
			GameObject.Find("Text6").GetComponent<TextMeshProUGUI>().text = currentText6;
			yield return new WaitForSeconds(delay);
			Audio = true;

			if(i == fullText[7].Length)
			{
				Audio = false;
			}
		}

		yield return new WaitForSeconds(delaytime);
		Audio = true;
		for(int i = 0; i <= fullText[7].Length; i++)
		{
			currentText7 = fullText[7].Substring(0,i);
			GameObject.Find("Text7").GetComponent<TextMeshProUGUI>().text = currentText7;
			yield return new WaitForSeconds(delay);
			
			if(i == fullText[7].Length)
			{
				StartCoroutine(LoadScene());
				Audio = false;
			}
		}
	}

	IEnumerator LoadScene()
	{
		yield return new WaitForSeconds(delaytime);
		GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;
	}
}
