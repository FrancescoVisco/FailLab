﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoLevelEditor : MonoBehaviour
{
    public float delaytime;

    void Awake()
    {
        StartCoroutine(StartDelayLoadScene());
    }

    void Update()
    {
        
    }
    
    IEnumerator StartDelayLoadScene()
    {
        yield return new WaitForSeconds(delaytime);
        GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;
    }
}
