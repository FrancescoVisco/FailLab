﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndCpu : MonoBehaviour
{
    public int i = 0;
    Vector3 startposition;

    void Start()
    {
        startposition = transform.position;
    }

    void Update()
    {
        if(i == 8)
        {
            GameObject.Find("LevelLoader").GetComponent<LevelLoader>().SceneToLoad = 5;
            GameObject.Find("LevelLoader").GetComponent<LevelLoader>().Fade = true;           
        }

        if(Input.GetButtonDown("Fire1") && GameObject.Find("TriggerAbility").GetComponent<Ability>().Object == this.gameObject)
        {
           transform.position = startposition + new Vector3(Random.Range(0,0.25f), 0 ,Random.Range(0,0.25f)); 
        }
    }
}
